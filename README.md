# Sir Bot a Lot

![](https://img.shields.io/pypi/v/sir-bot-a-log.svg)
![](https://img.shields.io/travis/autoferrit/sir-bot-a-lot.svg)
![](https://readthedocs.org/projects/sir-bot-a-lot/badge/?version=latest)
![](https://pyup.io/repos/gitlab/autoferrit/sir-bot-a-lot/shield.svg)

A slack bot built for the people and by the people of the python developers slack community. https://pythondev.slack.com/
Want to contribute?
Get an invite!
http://pythondevelopers.herokuapp.com/

* Documentation: https://sir-bot-a-lot.readthedocs.io
